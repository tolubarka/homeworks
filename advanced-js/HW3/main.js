// Прототипное наследование в Javascript - это возможность языка, которая помогает не копировать/переопределять методы объекта,
// а просто создать новый объект на его основе.


  class Employee {
    constructor(name, age, salary) {
      this.name = name;
      this.age = age;
      this.salary = salary;
    }
    get employeeName() {
      return this.name;
    }
    get employeeAge() {
      return this.age;
    }
    get employeeSalary() {
      return this.salary;
    }
  };

//   let employee = new Employee ()

// console.log(employee)

  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary, lang);
      this.lang = lang;
    }
    get programmerLang() {
      return this.lang;
    }
    get salaryInfo () {
      return this.salary * 3;
    }
   
  }
  let programmer = new Programmer("Andre", 15, 2000, "Chinese, English")

      console.log(programmer.salaryInfo);
      console.log(programmer.programmerLang);
      console.log(programmer);