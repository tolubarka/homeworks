// Прототипное наследование в Javascript - это возможность языка, которая помогает не копировать/переопределять методы объекта,
// а просто создать новый объект на его основе.
const asyncRequest = async (url) => {
  let response = await fetch(url);
  return await response.json();
};

const loadIpInfo = () => {
  asyncRequest('https://api.ipify.org/?format=json')
      .then((json) => asyncRequest(`http://ip-api.com/json/${json.ip}`))
      .then((json) =>
      document.querySelector(".on-1").insertAdjacentHTML('beforeend', 
      `<ul>
      <li> status: ${success}</li>
      <li> country: ${country}</li>
      <li> countryCode: ${region}</li>
      <li> region: ${city}</li>
      <li> regionName: ${zip}</li>
      </ul>`)
      // // document.querySelector(".on-1").append(ul)
      )};