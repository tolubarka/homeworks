
const user1 = {
  name: "John",
  years: 30
};

const {name: Name, years: ago, isAdmin: isAdmin = false} = user1;

alert(Name);
alert(ago);
alert(isAdmin);