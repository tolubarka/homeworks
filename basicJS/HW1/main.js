// let и const  почти одно и тоже. Но всё же есть
// отличие: значение переменной, объявленной с
// помощью const, нельзя переназначить. Разница
// между let и const в том, что в первом случае
// мы можем изменить значение переменной,
// а во втором нет.
     // Var более устаревший. Область видимости 
// var переменных ограничена функцией, 
// если мы обратимся к переменной до её
// объявления, то получим  undefined.
// Сonst и let ограничены блоком, попытка обратится
// к переменной до её объявления, вернётся
// ошибкой ReferenceError. В отличие от
// var, let не позволяет повторно объявлять
// переменную в рамках ее области видимости.

let name = prompt('Введите имя', '');
let age = +prompt('Сколько Вам лет', '');

// if (age < 18) {
//     alert('You are not allowed to visit this website');
// } else if (age <= 22) {
//     if (confirm('Are you sure you want to continue?')) {
//         alert('Welcome ' + name)   
//     } else {
//         alert('You are not allowed to visit this website');
//     } 
// } else {
//     alert('Welcome ' + name);
// }

if (age > 18 && age <= 22 && confirm('Are you sure you want to continue?') || age > 22) {
        alert('Welcome ' + name);   
    } else {
        alert('You are not allowed to visit this website');
    }