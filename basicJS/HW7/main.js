//  Это независящий от платформы и языка программный интерфейс,
// позволяющий программам и скриптам получить доступ к содержимому HTML-,
// XHTML- и XML-документов, а также изменять содержимое, структуру и оформление
// таких документов.

// function createList(arr, parent = document.body) { 
//   let ul = document.createElement("ul");

//     arr.map((item) => {
//       ul.insertAdjacentHTML("beforeend", `<li>${item}</li>`);
//     });
//       parent.append(ul);
// }  
//     createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);

let timer = document.querySelector(".timer");
let interval = setInterval(() => {
  if (+timer.innerHTML > 0) {
    timer.innerHTML -= 1;
  }
  if (+timer.innerHTML === 0) {
    document.body.innerHTML = '';
    clearInterval(interval);
  }
}, 1000);

let arr = [
  'Kharkiv',
  'Kiev',
  ['Borispol', 'Irpin', ["asfs", "ddfbhn", "hgufkj"]],
  'Odessa',
  'Lviv',
  'Dnieper',
];

function getListFromArray(someArr) {
  let newMas = someArr.map((item) => {
    if (Array.isArray(item)) {
      return getListFromArray(item);   
    } else {
      return `<li>${item}</li>`;
    }
  });
  return `<ul>${newMas.join("")}</ul>`;
}
document.body.insertAdjacentHTML("beforeend", getListFromArray(arr));

      