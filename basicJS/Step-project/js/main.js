let tab = function () {
    let tabNav = document.querySelectorAll(".tabs-nav"),
        tabContent = document.querySelectorAll(".tab"),
        tabName;
    tabNav.forEach(item => {
        item.addEventListener("click", selectTabNav)
    });

    function selectTabNav() {
        tabNav.forEach(item => {
            item.classList.remove("is-active");
        });
        this.classList.add("is-active");
        tabName = this.getAttribute("data-tab-name");
        selectTabContent(tabName);
    }
    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add("is-active") :
            item.classList.remove("is-active");
        });
    }
};
tab();


let box_img = function () {
    let tabNav = document.querySelectorAll(".list"),
        tabContent = document.querySelectorAll(".box-img"),
        tabName;
    tabNav.forEach(item => {
        item.addEventListener("click", selectTabNav)
    });

    function selectTabNav() {
        tabNav.forEach(item => {
            item.classList.remove("is-active");
        });
        this.classList.add("is-active");
        tabName = this.getAttribute("data-tab-name");
        selectTabContent(tabName);
    }
    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            !item.classList.contains(tabName)
        ? item.classList.add("hide")
        : item.classList.remove("hide");
        });
    }
};
box_img();


function loadImages() {
    const data = [
        {
            path: './img/amazing-work-img/design/graphic-design1.jpg',
            class: 'wordpress',
        },
        {
            path: './img/amazing-work-img/design/graphic-design2.jpg',
            class: 'design',
        },
        {
            path: './img/amazing-work-img/design/graphic-design3.jpg',
            class: 'wordpress',
        },
        {
            path: './img/amazing-work-img/design/graphic-design4.jpg',
            class: 'landing-pages',
        },
        {
            path: './img/amazing-work-img/design/graphic-design5.jpg',
            class: 'web-design',
        },
        {
            path: './img/amazing-work-img/design/graphic-design6.jpg',
            class: 'landing-pages',
        },
        {
            path: './img/amazing-work-img/design/graphic-design7.jpg',
            class: 'design',
        },
        {
            path: './img/amazing-work-img/design/graphic-design8.jpg',
            class: 'landing-pages',
        },
        {
            path: './img/amazing-work-img/design/graphic-design9.jpg',
            class: 'web-design',
        },
        {
            path: './img/amazing-work-img/design/graphic-design10.jpg',
            class: 'web-design',
        },
        {
            path: './img/amazing-work-img/design/graphic-design11.jpg',
            class: 'design',
        },
        {
            path: './img/amazing-work-img/design/graphic-design1.jpg',
            class: 'design',
        }
    ];
    data.forEach(item => {
        document.getElementById('divImages').innerHTML
            += `<div class="box-img all ${item.class}">
                <img class="picture" src="${item.path}" style="width:285px;">
                    <div class="transparent-block">
                        <div class="burger burger-top">
                            <span class="burger-line"></span>
                        </div>
                    <div class="box-icon-img">
                        <img src="./img/icon/icon.png" alt="">
                    </div>
                    <div class="text-transparent">
                        <h1 class="line-green">CREATIVE DESIGN</h1>
                        <span class="line-gray">Web Design</span>
                    </div>
                </div>
            </div>`;
    });
    document.getElementById('load_more').style.visibility="hidden";
box_img();
}


$(document).ready(function() {
    $('.big-slider').slick({
        arrows:false,
        slidesToShow:1,
        slidesToScroll:1,
        autoplay: true,
        autoplaySpeed:2000,
        infinite:true,
        asNavFor: '.slider',
    });
});

$(document).ready(function() {
    $('.slider').slick({
        arrows:true,
        slidesToShow:4,
        slidesToScroll:1,
        autoplay: true,
        autoplaySpeed:2000,
        infinite:true,
        focusOnSelect:true,
        asNavFor: '.big-slider',
    });
});