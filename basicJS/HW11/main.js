// Для работы с input не рекомендуется использовать события клавиатуры
// потому как на разных раскладках к одной и той же клавише могут быть
// привязаны разные символы.


const Keyboard = [13, 83, 69, 79, 78, 76, 90];

document.onkeyup = function (event) {
    if (!Keyboard.includes(event.keyCode)) {
        return;
    }
    document.querySelectorAll('.btn').forEach(function(element) {
        element.classList.remove('active');
    });
    document.querySelector('.btn[data-name="' + event.keyCode + '"]').classList.add('active');
}