//     Объект — это набор свойств, и каждое свойство состоит из имени и значения, 
// которое ассоциируется с этим именем. Значением свойства может быть функция, 
// которую можно назвать методом объекта.


let firstname = prompt("Шаг 1. Введите ваше иммя",'');
let lastname = prompt("Шаг 2. Введите вашу фамилию",'');


function createNewUser(firstname, lastname) {
    return {
        firstname: firstname,
        lastname: lastname,
        getLogin: function () {
            return firstname.charAt(0) + lastname;
        }
    }
}

let newUser = createNewUser(firstname, lastname);
console.log(newUser.getLogin());