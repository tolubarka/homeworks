// Рекурсию используют, когда вычисление функции можно свести к её более простому вызову, 
// а его – к ещё более простому и так далее, пока значение не станет очевидно.



function factorial(n) {
    return n ? n * factorial(n - 1) : 1;
}

let number, inputNumber = '';

do {
    inputNumber = prompt('Введите целое положительное число [0...100]:', inputNumber);
    number = +inputNumber;
} while (!Number.isInteger(number) || number < 0 || number > 100);

// console.log(factorial(number));
document.body.innerHTML(factorial(number));