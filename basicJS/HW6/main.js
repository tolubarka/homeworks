//  Метод forEach используется для перебора массива. Он для каждого элемента массива
// вызывает функцию callback. Этой функции он передаёт три параметра callback(item, i, arr)

function filterBy(data, type) {
    let arr = [];
    data.forEach(function(item) {
        if (typeof item !== type) {
            arr.push(item);
        }
        
    });
    return arr;
}
let all = ['hello', 'world', 23, '23', null];

console.log(filterBy(all, 'string'));

    
