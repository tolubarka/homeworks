//1) setTimeout позволяет вызвать функцию один раз через определённый интервал времени.
// setInterval позволяет вызывать функцию регулярно,
// повторяя вызов через определённый интервал времени.
// 3) важно не забывать вызывать функцию clearInterval(),
// когда ранее созданный цикл запуска нам уже не нужен
// потому как setInterval будет работать бесконечно

let images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg',];

let slider = document.querySelector("#slider");
let img = slider.querySelector('img');

let i = 1; 
img.src = 'img/' + images[0];
    let timerId = window.setInterval(function() {
        img.src = 'img/' + images[i];
        i++
        if (i == images.length) {
            i = 0;
        }
    }, 3000);

document.querySelector(".btn").addEventListener('click', function () {
    clearInterval(timerId);
});

document.querySelector(".btn-start").addEventListener('click', function () {
    timerId = window.setInterval(function() {
        img.src = 'img/' + images[i];
        i++
        if (i == images.length) {
            i = 0;
        }
    }, 3000);
});
 