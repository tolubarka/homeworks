if (!localStorage.thema) localStorage.thema = "light"
document.body.className = localStorage.thema
btnLocalStorage.innerText = document.body.classList.contains("dark") ? "Сменить светлую тему" : "Сменить темную тему"

btnLocalStorage.onclick = () => {
    document.body.classList.toggle("dark")
    btnLocalStorage.innerText = document.body.classList.contains("dark") ? "Сменить светлую тему" : "Сменить темную тему"
    localStorage.thema = document.body.className || "light"
};