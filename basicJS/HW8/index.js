let priceInput = document.querySelector(".price-input");
let priceText = document.querySelector(".price-text");
let inputError = document.querySelector(".input-error");

priceInput.onblur = function () {
  if (priceInput.value.match(/[-a-zA-Zа-яА-Я]/)) {
    priceInput.classList.add("Invalid");
    inputError.innerHTML = "Please enter correct price.";
  } else {
    let inputData = priceInput.value;
    let priceBox = document.querySelector(".price-box");
    priceBox.insertAdjacentHTML(
      "afterbegin",
      `<div class="price-list"><span class="price-text">
        ${inputData}
      </span><button class="price-text-btn">x</button></div>`
    );
    this.style.color = "green";
    let delBtn = document.querySelector(".price-text-btn");
    delBtn.onclick = function () {
      delBtn.parentNode.remove();
      priceInput.value = "";
    };
  }
};

priceInput.onfocus = function () {
  if (this.classList.contains("Invalid")) {
    this.classList.remove("invalid");
    inputError.innerHTML = "";
  }
  priceInput.style.outline = "green";
};
