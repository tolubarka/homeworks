//     Функции в программировании нужны для того,
// чтобы вывести повторяющие фрагменты кода для
// переиспользования (можно с другими параметрами);
    // Функция возвращает результат на основании
    // параметров.

    let a, b, inputA = '', inputB = '', operation = '';
    const availableOperations = ['+', '-', '*', '/'];
    
    do {
        inputA = prompt('Введите первое число', inputA);
        a = +inputA;
    } while (Number.isNaN(a));
    
    do {
        inputB = prompt('Введите второе число', inputB);
        b = +inputB;
    } while (Number.isNaN(b));
    
    do {
        operation = prompt('Какую математическиую операцию вы хотите совершить? [' + availableOperations.join(', ') + ']', operation);
    } while (!availableOperations.includes(operation));
    
    function calculate(operand1, operand2, operation) {
        switch (operation) {
            case '+':
                return operand1 + operand2;
            case '-':
                return operand1 - operand2;
            case '*':
                return operand1 * operand2;
            case '/':
                return operand1 / operand2;
            default:
                return null;
        }
    }
    
    console.log(calculate(a, b, operation));
    