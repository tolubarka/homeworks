//  Это независящий от платформы и языка программный интерфейс,
// позволяющий программам и скриптам получить доступ к содержимому HTML-,
// XHTML- и XML-документов, а также изменять содержимое, структуру и оформление
// таких документов.

let tab = function () {
    let tabNav = document.querySelectorAll(".tabs-title"),
        tabContent = document.querySelectorAll(".tab"),
        tabName;
    tabNav.forEach(item => {
        item.addEventListener("click", selectTabNav)
    });

    function selectTabNav() {
        tabNav.forEach(item => {
            item.classList.remove("active");
        });
        this.classList.add("active");
        tabName = this.getAttribute("data-tab-name");
        selectTabContent(tabName);
    }
    function selectTabContent(tabName) {
        tabContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add("active") :
            item.classList.remove("active");
        });
    }
};
tab();