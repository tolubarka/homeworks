//  Циклы в програмировании используются
// для выполнения ряда повторяющихся команд.
//  Упростить написание кода для повторяющихся
// действий

// let multiples = +prompt('Введите число', '5');

// if (multiples < 5) {
//     alert ('Sorry, no numbers');
// }
// for (let i = 1; i <= multiples; i++) {
//     if (i % 5 === 0) {
//         console.log(i)
//     }
// }



let m, n;

do {
    m = +prompt("Шаг 1. Введите число (оно должно быть целым и больше 0):");
} while (!Number.isInteger(m) || m <= 0);

do {
    n = +prompt("Шаг 2. Введите число (оно должно быть целым и больше предыдущего):");
} while (!Number.isInteger(n) || n <= m);


function isPrime (number) {

  for (let i = 2; i < number; i++) {
    if (number % i === 0) {
      return false;
    }
  }
      return true;
}

function primeNumbers (m, n) {
  let primes = [];
      for (let i = m; i <= n; i++) {
        if (isPrime(i)) {
          primes.push(i);
        }
      }
      return primes;
    }
console.log(primeNumbers(m, n));

