
function fibonacci(n) {
  if (n > 1) {
      // для положительных чисел, кроме 0 и 1
      return fibonacci(n - 1) + fibonacci(n - 2);
  } else if (n < 0) {
      // для отрицательных чисел
      return fibonacci(n + 2) - fibonacci(n + 1);
  }
  // для 0 и 1
  return n;
}

let n = +prompt('Введите целое число:');

console.log(fibonacci(n));

      