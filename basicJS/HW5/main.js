//     Экранирование нужно для того, что бы работали спец символы,
// и мы могли ввести какой-то символ в строчку, что бы он не воспринимается 
// как часть скрипта.


let firstname = prompt('Шаг 1. Введите ваше иммя', 'Ivan');
let lastname = prompt('Шаг 2. Введите вашу фамилию', 'Kravchenko');
let birthdayString = prompt('Шаг 3. Введите вашу дату рождения', 'dd.mm.yyyy');
let birthdayArray = birthdayString.split('.');
let birthday = new Date(birthdayArray[2], birthdayArray[1], birthdayArray[0]);

function createNewUser(firstname, lastname, birthday) {
    return {
        firstname: firstname,
        lastname: lastname,
        birthday: birthday,
        getPassword: function () {
            return this.firstname.charAt(0).toUpperCase()
                + this.lastname.toLowerCase()
                + this.birthday.getFullYear();
        },
        getAge: function () {
            return (new Date()).getFullYear() - this.birthday.getFullYear();
        }
    }
}

let newUser = createNewUser(firstname, lastname, birthday);
console.log(newUser.getAge(), newUser.getPassword());