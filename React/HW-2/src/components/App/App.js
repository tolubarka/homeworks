import React from "react";

import Modal from "../Modal";
import Button from "../Button";
import Actions from "../Actions";
import Cards from "../Cards";

import "./App.scss";

class App extends React.PureComponent {
  state = {
    firstModal: false,
    secondModal: false,
  };

  firstModalHandler = () => {
    this.setState((state) => {
      return {
        ...state,
        firstModal: !state.firstModal,
        secondModal: false,
      };
    });
  };

  secondModalHandler = () => {
    this.setState((state) => {
      return {
        ...state,
        secondModal: !state.secondModal,
        firstModal: false,
      };
    });
  };

  render() {
    return (
      <div className="App">
        <Button
          text="Open first modal"
          bgc="#FFD649"
          onClick={this.firstModalHandler}
        ></Button>
        <Button
          text="Open second modal"
          bgc="#2C2875"
          onClick={this.secondModalHandler}
        ></Button>

        {this.state.firstModal && (
          <Modal
            text="Once you delete this file, it won’t be possible to undo this action. 
            Are you sure you want to delete it?"
            header="Do you want to delete this file?"
            closeButton={true}
            closeModalHandler={this.firstModalHandler}
            actions={[
              <Actions actiontext="Ok"></Actions>,
              <Actions actiontext="Cancel"></Actions>,
            ]}
            modbgc="#FFD649"
          ></Modal>
        )}

        {this.state.secondModal && (
          <Modal
            text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem nulla nisi ipsum, eveniet, "
            header="Do you want to create file?"
            closeButton={true}
            closeModalHandler={this.secondModalHandler}
            actions={[
              <Actions actiontext="Create"></Actions>,
              <Actions actiontext="Cancel"></Actions>,
              <Actions actiontext="Ok"></Actions>,
            ]}
            modbgc="#2C2875"
          ></Modal>
        )}
        {/* <Cards></Cards> */}
      </div>
    );
  }
}
export default App;
