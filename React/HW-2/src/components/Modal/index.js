import React from "react";
import btnClose from "../../icons/close.svg";

class Modal extends React.PureComponent {
  render() {
    const {
      header,
      modbgc,
      closeButton,
      text,
      actions,
      closeModalHandler,
    } = this.props;
    return (
      <div className="container">
        <div
          className="modal-container"
          onClick={(e) => {
            e.target === e.currentTarget && closeModalHandler();
          }}
        >
        <div
          className="modal"
          style={{
            backgroundColor: modbgc,
          }}
        >
          <div className="modal__header">
            <span className="modal__header__text">{header}</span>{" "}
            {closeButton && (
              <img
                className="modal__btn__close"
                onClick={() => {
                  closeModalHandler();
                }}
                src={btnClose}
                alt="img"
              ></img>
            )}
            </div>
            <p className="modal__text">{text}</p>
            <div className="modal__actions">{actions}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
