const Button = ({ bgc, text, onClick = () => {} }) => {
  return (
    <button className="btnopen"
      onClick={() => {
        onClick();
      }}
      style={{
        backgroundColor: bgc,
      }}
    >
      {text}
    </button>
  );
};

export default Button;
